package learninghibernate;

import java.util.Date;
import java.util.List;

public class RunHibernate {
    public static void main(String[] args) {

        //Creates the SessionFactory used in the program.
        DataAccessObject dao = DataAccessObject.getInstance();
        Date added = new Date();

        //Adds three entries to the heros table.
        dao.addHero("test", "test", "human");
        dao.addHero("test", "test", "human");
        Integer hID3 =  dao.addHero("test3", "test", "human");

        //List all entries in the heros table
        List<Hero> h = dao.getHeros();
        for (Hero i : h) {
            System.out.println(i);
        }

        //Prints the first entry in the table
        System.out.println(dao.getHero(1));

        //Deletes the third item that was added earlier
        dao.deleteHero(hID3);

        //List all entries in the heros table
        h = dao.getHeros();
        for (Hero i : h) {
            System.out.println(i);
        }
    }
}
