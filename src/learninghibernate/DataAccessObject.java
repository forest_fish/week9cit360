package learninghibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.hql.internal.ast.HqlASTFactory;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class DataAccessObject {

    SessionFactory factory;
    Session session = null;

    private static DataAccessObject single_instance = null;

    private DataAccessObject() { factory = FactoryMaker.getSessionFactory(); }

    public static DataAccessObject getInstance() {

        if (single_instance == null) {
            single_instance = new DataAccessObject();
        }

        return single_instance;
    }

    //Retrieves all entries in a table
    public List<Hero> getHeros() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from learninghibernate.Hero";
            List<Hero> heroList = (List<Hero>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return heroList;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    //Gets a single entry from the table based on id
    public Hero getHero (int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from learninghibernate.Hero where id=" + Integer.toString(id);
            Hero h = (Hero)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return h;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    //Adds and entry the the table
    public Integer addHero(String firstName, String lastName, String race){
        Integer id = null;
        Date added = new Date();

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Hero hero = new Hero(firstName, lastName, race, added);
            id = (Integer) session.save(hero);
            hero.setId(id);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction()!=null) session.getTransaction().rollback();
        } finally {
            session.close();
        }
        return id;
    }

    //deletes an entry from the table based on id
    public void deleteHero(Integer id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Hero hero = (Hero)session.get(Hero.class, id);
            session.delete(hero);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction() != null) session.getTransaction().rollback();
        }
    }
}
