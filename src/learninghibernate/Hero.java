package learninghibernate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "heros")
public class Hero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "race")
    private String race;

    @Column(name = "added")
    private Date added;

    public Hero(String firstName, String lastName, String race, Date added) {
        setFirstName(firstName);
        setLastName(lastName);
        setRace(race);
        setAdded(added);
    }

    public Hero() {
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getRace() {
        return race;
    }

    public Date getAdded() {
        return added;
    }

    public String toString() {
        return Integer.toString(id) + " " + firstName + " " + lastName + " " + race + " " + added;
    }
}